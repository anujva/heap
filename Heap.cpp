#include "Heap.h"

template <class T>
Heap<T>::Heap(int N, bool (*compare1)(T &obj1, T &obj2)){
  id.resize(N);
  maxSize = N;
  compare = compare1;
  currentSize = 0;
}

template <class T>
void Heap<T>::heapify(){
  int index = currentSize-1;
  while(index > 0){
    if(index%2 == 0){
      int parentIndex = (index-2)/2;
      if(compare(id[parentIndex], id[index])){
        swap(id[parentIndex], id[index]);
      }
    }else{
      int parentIndex = (index - 1)/2;
      if(compare(id[parentIndex], id[index])){
        swap(id[parentIndex], id[index]);
      }
    }
    index --;
  }
}

template <class T>
void Heap<T>::swap(T &obj1, T &obj2){
  T temp = obj1;
  obj1 = obj2;
  obj2 = temp;
}

template <class T>
void Heap<T>::insert(T obj){
  if(currentSize != maxSize){
    id[currentSize++] = obj;
    heapify();
  }else{
    cout<<"The heap is full and needs to be resized if you want to add more items"<<endl;
  }
}

template <class T>
void Heap<T>::printHeap(){
  cout<<"The current state of the heap"<<endl;
  for(int i=0; i<currentSize; i++){
    cout<<id[i]<<" ";
  }
  cout<<endl;
}

bool comparisionFunction(int &a, int &b){
  if(a<b){
    return true;
  }else{
    return false;
  }
}

template <class T>
T Heap<T>::extractMax(){
  if(currentSize > 0){
    T maxVal = id[0];
    id.erase(id.begin());
    currentSize--;
    heapify();
    return maxVal;
  }else{
    cout<<"The heap is empty"<<endl;
  }
}

template <class T>
T Heap<T>::getMax(){
  if(currentSize > 0){
    return id[0];
  }else{
    cout<<"The heap is empty"<<endl;
  }
}

int main(){
  Heap<int> heap(10, &comparisionFunction);
  heap.insert(8);
  heap.insert(5);
  heap.insert(9);
  heap.insert(10);
  heap.insert(11);
  heap.insert(21);
  heap.insert(51);
  heap.insert(32);
  heap.insert(29);
  heap.printHeap();
  int maxVal = heap.extractMax();
  cout<<endl<<"The max value extracted from the Heap is : "<<maxVal;
  cout<<endl;
  heap.printHeap();
  cout<<endl<<"The currentMax in the heap is : "<<heap.getMax();
}
