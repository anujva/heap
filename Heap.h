#include <iostream>
#include <vector>

using namespace std;

template <class T>
class Heap{
  private:
    vector<T> id;
    bool (*compare)(T &obj1, T &obj2);
    int currentSize;
    int maxSize;
    void swap(T &obj1, T &obj2);

  public:
    Heap(int N, bool (*compare1)(T &obj1, T &obj2));
    void heapify();
    void insert(T obj);
    T extractMax();
    T getMax();
    void printHeap();
};
